# Teste de desenvolvimento Front End

Layout disponível em `https://www.figma.com/file/q7ENhEoeaqFmaTFHWbPOFh/teste-front-end?node-id=0%3A1`

A proposta do teste é:

- Criar uma landing page _responsiva_ do projeto fictício **_Digital Game House_**
- Na seção **Our Games**, criar um arquivo JSON a parte com os jogos que você mais gosta :) (pode usar o (json-server)[https://www.npmjs.com/package/json-server])
- Em **see more** abrir um modal com a listagem de todos os jogos que você adicionou no seu arquivo JSON. (`use sua imaginação`)
- Na seção **Our Services**, adicionar imagens ou descrição (`use sua imaginação`)
- Ao clicar num jogo, abrir um modal com nome, data de publicação e descrição (`use sua imaginação`)

### Pontuaremos:

- Estrutura de projeto
- Genericidade de código (componentes reutilizáveis)
- Patterns
- Organização de CSS
- Responsividade

### Requisitos:

- Desenvolver em Javascript, nativo ou utilizando qualquer framework da linguagem. (ReactJS pontuará mais)

### Entrega:

- Faça um _**fork**_ deste repositório
- Desenvolva sua aplicação
- Envie as atualizações para seu repositório
- Faça um _**pull request**_
- Envie-nos um e-mail confirmando a finalização
